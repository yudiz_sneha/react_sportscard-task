import React from 'react';
import PropTypes from 'prop-types';
import './SmallCard.css';

function SmallCard({ data }) {
	return (
		<div className='small-card'>
			
			<img className='img' src={data?.oImg.sUrl} />

			<div className='title'>
				<a href='#'>{data?.sTitle}</a>
			</div>
			
		</div>
	)
}

SmallCard.propTypes = {
	data: PropTypes.object,
}
export default SmallCard
