import React from 'react';
import PropTypes from 'prop-types';
import './GridCard.css';

function GridCard({ data }) {
	return (
		<div className='card'>

			<img className='img' src={data?.oImg.sUrl} />

			<div className='title'>
				<a href='#'>{data?.sTitle}</a>
			</div>
			
		</div>
	)
}

GridCard.propTypes = {
	data: PropTypes.object,
}

export default GridCard
