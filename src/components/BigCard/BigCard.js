import React from 'react';
import PropTypes from 'prop-types';
import './BigCard.css';

function BigCard({ data }) {

	return (
		<div className='big-card'>

			<img className='img' src="https://www.crictracker.com/wp-content/uploads/2018/05/Chennai-Super-Kings-team.jpg" style={{ width: "100%" }}/>
			
			<div className='title'>
				<a href='#'>{data?.sTitle}</a>
				<p className='description'>{data?.sDescription}</p>
			</div>
			
		</div>
	)
}

BigCard.propTypes = {
	data: PropTypes.object,
}

export default BigCard;
